import pickle
from pathlib import Path

import click
import dvc.api
import pandas as pd

from sklearn.feature_extraction.text import TfidfVectorizer

def train_tf_idf_vect(data):
    params = dvc.api.params_show()
    vect_params = params["vectorizer_tfidf"]
    print(f"Train TfidfVectorizer with params: {vect_params}")
    vect = TfidfVectorizer(**vect_params)
    vect.fit(data["corpus"].str.join(" "))
    return vect

@click.command()
@click.argument("input_path_train", type=click.Path(exists=True))
@click.argument("output_path_vect", type=click.Path())
def cli_train_vectorize(input_path_train: Path, output_path_vect: Path):

    print(f"Read input data: {input_path_train}")
    data = pd.read_parquet(input_path_train)

    vect = train_tf_idf_vect(data)

    print(f"Save TfidfVectorizer to: {output_path_vect}")
    with open(output_path_vect, "wb") as f_out:
        pickle.dump(vect, f_out)

if __name__ == '__main__':
    cli_train_vectorize()