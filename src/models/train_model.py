import pickle
from pathlib import Path

import click
import dvc.api
import numpy as np
import pandas as pd
import scipy

from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier

MODELS = {'lr': 'LogisticRegression',
          'rf': 'RandomForestClassifier'}

@click.command()
@click.argument("input_path_data", type=click.Path(exists=True))
@click.argument("input_path_target", type=click.Path(exists=True))
@click.argument('model_type', type=click.Choice(MODELS.keys()))
@click.argument("output_path_model", type=click.Path())
def cli_train_model(input_path_data: Path, 
                        input_path_target: Path,
                        model_type: str,
                        output_path_model: Path):
    
    print(f"Read input data: {input_path_data}")
    X_train = scipy.sparse.load_npz(input_path_data)

    print(f"Read input target: {input_path_target}")
    y_train = np.load(input_path_target)

    model_name = MODELS[model_type]
    print(f"Train {model_name}")

    params = dvc.api.params_show()
    if model_type == 'lr':
        model = LogisticRegression(**params["logistic_regression"])
    elif model_type == 'rf':
        model = RandomForestClassifier(**params["random_forest"])
    
    model.fit(X_train, y_train)

    print(f"Save model to file: {output_path_model}")
    with open(output_path_model, "wb") as f_out:
        pickle.dump(model, f_out)

if __name__ == '__main__':
    cli_train_model()