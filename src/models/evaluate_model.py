import pickle
from pathlib import Path

import click
import numpy as np
import pandas as pd
import scipy
import json

from sklearn.metrics import classification_report, roc_auc_score, confusion_matrix

MODELS = {'lr': 'LogisticRegression',
          'rf': 'RandomForestClassifier'}

@click.command()
@click.argument("input_path_data", type=click.Path(exists=True))
@click.argument("input_path_target", type=click.Path(exists=True))
@click.argument("input_path_model", type=click.Path(exists=True))
@click.argument("output_path_metric", type=click.Path())
def cli_evaluate_model(input_path_data: Path, 
                        input_path_target: Path,
                        input_path_model: Path,
                        output_path_metric: Path):
    
    print(f"Read input data: {input_path_data}")
    X_test = scipy.sparse.load_npz(input_path_data)

    print(f"Read input target: {input_path_target}")
    y_test = np.load(input_path_target)

    print(f"Load model: {input_path_model}")
    with open(input_path_model, "rb") as f_in:
        model = pickle.load(f_in)

    print(f"Evaluate model...")
    y_pred = model.predict(X_test)
    y_pred_proba = model.predict_proba(X_test)[:,1]

    report = classification_report(y_test, y_pred, output_dict=True)
    report['roc_auc'] = roc_auc_score(y_test, y_pred_proba)

    tn, fp, fn, tp = confusion_matrix(y_test, y_pred).ravel().astype(float)
    report['confusion_matrix'] = {'TP': tp, 
                                  "TN": tn, 
                                  "FP": fp, 
                                  "FN":fn}

    print(report)

    print(f"Save metrics to file: {output_path_metric}")
    with open(output_path_metric, "w") as f_out:
        json.dump(report, f_out, indent=2)

if __name__ == '__main__':
    cli_evaluate_model()