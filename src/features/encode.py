import pickle
from pathlib import Path

import click
import numpy as np
import pandas as pd
import scipy


@click.command()
@click.argument("input_path_data", type=click.Path(exists=True))
@click.argument("input_path_vect", type=click.Path())
@click.argument("output_path_data", type=click.Path())
@click.argument("output_path_target", type=click.Path(), required=0)
def cli_encode_data(input_path_data: Path, input_path_vect: Path, 
                    output_path_data: Path, output_path_target: Path):

    print(f"Read input data: {input_path_data}")
    data = pd.read_parquet(input_path_data)

    print(f"Read vectorizer: {input_path_vect}")
    with open(input_path_vect, "rb") as f_in:
        vect = pickle.load(f_in)
 
    print(f"Encoding data")
    X = vect.transform(data["corpus"].str.join(" "))
    
    print(f"Save data to file: {output_path_data}")
    scipy.sparse.save_npz(output_path_data, X)

    if output_path_target is not None:
        print(f"Save target to file: {output_path_target}")
        np.save(output_path_target, data["polarity"])

if __name__ == '__main__':
    cli_encode_data()