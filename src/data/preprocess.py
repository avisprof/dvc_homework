import re
from pathlib import Path

import click
import dvc.api
import pandas as pd

import nltk
from nltk.corpus import stopwords
from nltk.stem import WordNetLemmatizer
nltk.download('stopwords')
nltk.download('wordnet')

def text_preprocessing(input_text: str) -> str:
    text = input_text.lower()
    text = re.sub(# remove hrefs 
        r"https?://\S+|www\.\S+|\[.*?\]|[^a-zA-Z\s]+|\w*\d\w*", "", text
    ) 
    text = re.sub("[0-9\-_]+","", text) # clear wildcards
    text = " ".join( # remove stopwords
        [word for word in text.split() if word not in stopwords.words('english')]
    )

    return text.strip()

def preprocess_data(data: pd.DataFrame, column: str) -> pd.DataFrame:

    # create new column
    data['corpus'] = data[column].apply(text_preprocessing).str.split(" ")

    # apply lemmatizer for new column
    lemmatizer = WordNetLemmatizer()
    data['corpus'].apply(lambda x: [lemmatizer.lemmatize(word) for word in x])

    return data


@click.command()
@click.argument("input_path", type=click.Path(exists=True))
@click.argument("output_path", type=click.Path())
@click.argument("column", type=click.STRING, default="review")
def cli_preprocess(input_path: Path, output_path: Path, column: str):

    params = dvc.api.params_show()
    n_rows=params["n_rows"]
    print(f"Read input data: {input_path}, nrows={n_rows}")
    
    data = pd.read_csv(input_path, header=None, nrows=n_rows)
    data.columns = ['polarity', 'title', 'review']
    
    print(f"Start preprocess of column: {column}")
    processed_data = preprocess_data(data, column)

    print(f"Write result to file: {output_path}")
    processed_data.to_parquet(output_path)

if __name__ == '__main__':
    cli_preprocess()
