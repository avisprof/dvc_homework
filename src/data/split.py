from pathlib import Path

import click
import dvc.api
import pandas as pd

from sklearn.model_selection import train_test_split

@click.command()
@click.argument("input_path", type=click.Path(exists=True))
@click.argument("output_path_train", type=click.Path())
@click.argument("output_path_test", type=click.Path())
def cli_split_data(input_path: Path, output_path_train: Path, output_path_test: str):

    print(f"Read input data: {input_path}")
    params = dvc.api.params_show()
    data = pd.read_parquet(input_path)

    print(f"Split data by train/test")
    data_train, data_test = train_test_split(data, 
                                             test_size=params["test_train_split"],
                                             shuffle=True,
                                             random_state=params["random_state"])

    print(f"Save data train to file: {output_path_train}")
    data_train.to_parquet(output_path_train)

    print(f"Save data test to file: {output_path_test}")
    data_test.to_parquet(output_path_test)

if __name__ == '__main__':
    cli_split_data()